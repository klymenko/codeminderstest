/***** ab
 * This is a test program with 5 lines of code
 *  \/* no nesting allowed!
 //*****//***/ class HiddenClass {}
public class Hello {
    public static final void main(String [] args) { // gotta love Java
        // Say hello
        System./*wait*/out./*for*/println/*it*/("Hello/*");
    }
}