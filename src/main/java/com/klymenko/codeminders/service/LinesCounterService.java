package com.klymenko.codeminders.service;

import com.klymenko.codeminders.utils.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

class BooleanWrapper {
    public boolean hasValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    private boolean value;
}
public class LinesCounterService {

    private static final String SINGLE_LINE_COMMENT = "//";
    private static final String MULTI_LINE_COMMENT_START = "/*";
    private static final String MULTI_LINE_COMMENT_END = "*/";

    private static final String NEW_LINE = "\n";
    private static final String TAB = "\t";

    private static final String JAVA_EXTENSION = ".java";


    // is it enough to use long? or just hypothetical BigInt?
    public static String countLines(String inputPath) {
        try {
            List<String> resultingOutput = new LinkedList<>();
            countLines(new File(inputPath), resultingOutput, 0);
            Collections.reverse(resultingOutput);
            return resultingOutput.stream().collect(Collectors.joining(NEW_LINE));
        } catch(IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public static long countLines(File file, List<String> resultingOutput, int tabTimes) throws IOException {
        BooleanWrapper openCommentsCounterSeen = new BooleanWrapper();
        long result = 0;
        if(file.isDirectory()) {
            for(File directoryFile: file.listFiles()) {
                result += countLines(directoryFile, resultingOutput, tabTimes + 1);
            }
        } else {
            if(file.getName().endsWith(JAVA_EXTENSION)) {
                result += Files.readAllLines(file.toPath())
                        .stream()
                        .map(it -> isMeaningfulLine(it, openCommentsCounterSeen))
                        .filter(it -> it)
                        .count();
            }
        }
        if(result != 0) {
            resultingOutput.add(StringUtils.repeat(TAB, tabTimes) + "" + file.getName() + " : " + result);
        }
        return result;
    }
    private static boolean isMeaningfulLine(String fileLine, BooleanWrapper openCommentSeen) {
        String unparsedFileLine = fileLine.trim();
        if(unparsedFileLine.isEmpty()) {
            return false;
        }
        int indxOfMultilineComment = unparsedFileLine.indexOf(MULTI_LINE_COMMENT_START);
        if(openCommentSeen.hasValue()) {
            int endMultilineCommentIndex = unparsedFileLine.indexOf(MULTI_LINE_COMMENT_END);
            if(endMultilineCommentIndex != -1) {
                openCommentSeen.setValue(false);
                return isMeaningfulLine(unparsedFileLine.substring(endMultilineCommentIndex + MULTI_LINE_COMMENT_END.length()), openCommentSeen);
            }
        } else if(!unparsedFileLine.startsWith(SINGLE_LINE_COMMENT) && indxOfMultilineComment != 0) {
            return true;
        } else if(unparsedFileLine.startsWith(MULTI_LINE_COMMENT_START)) {
            openCommentSeen.setValue(true);
            return isMeaningfulLine(unparsedFileLine.substring(MULTI_LINE_COMMENT_END.length()), openCommentSeen);
        } else if(unparsedFileLine.startsWith(MULTI_LINE_COMMENT_END)) {
            openCommentSeen.setValue(false);
            return isMeaningfulLine(unparsedFileLine.substring(MULTI_LINE_COMMENT_END.length()), openCommentSeen);
        }
        return false;
    }
}
