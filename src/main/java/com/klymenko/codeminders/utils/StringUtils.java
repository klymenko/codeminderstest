package com.klymenko.codeminders.utils;

public class StringUtils {

    // well, maybe it's time to upgrade jdk to 11+ ? )
    public static String repeat(String value, int times) {
        StringBuilder result = new StringBuilder();
        for (int j = 0; j < times; j++)
            result.append(value);
        return result.toString();
    }
}
