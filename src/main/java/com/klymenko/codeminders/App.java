package com.klymenko.codeminders;

import com.klymenko.codeminders.service.LinesCounterService;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        System.out.println("Welcome to lines counter app.");

        Scanner sc = new Scanner(System.in);
        String usageRulesText = "Please provide file/directory path as an argument and you'll see total amount of valid lines inside. Use 'q' input to quit";

        System.out.println(usageRulesText);

        String input = "";
        while(true) {
            input = sc.nextLine();
            if(input.equals("q")) break;
            try {
                System.out.println(LinesCounterService.countLines(input));
                // some formatting, huh?
                System.out.println("\n");
                System.out.println(usageRulesText);
            } catch(RuntimeException e) {
                System.out.println("There's an error in your input: " + e);
                System.out.println("Please, try again");
            }
        }
    }
}
