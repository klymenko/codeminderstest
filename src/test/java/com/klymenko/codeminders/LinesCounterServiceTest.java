package com.klymenko.codeminders;

import com.klymenko.codeminders.service.LinesCounterService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class LinesCounterServiceTest {

    private LinesCounterService linesCounterService = new LinesCounterService();
    private List<String> FAKE_LIST = new LinkedList<>();

    @Test
    public void testEmptyFile() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            linesCounterService.countLines("nonexistingfile");
        });
    }

    @Test
    public void testDaveExample() throws IOException {
        Assertions.assertEquals(3, linesCounterService.countLines(new File("test-classes/subfolder2/Dave.java"), FAKE_LIST, 0));
    }

    @Test
    public void testHelloExample() throws IOException {
        Assertions.assertEquals(5, linesCounterService.countLines(new File("test-classes/subfolder2/Hello.java"), FAKE_LIST, 0));
    }

    @Test
    public void testCustomStrangeCommentsExample() throws IOException {
        Assertions.assertEquals(6, linesCounterService.countLines(new File("test-classes/subfolder1/CustomStrangeComments.java"), FAKE_LIST, 0));
    }

    @Test
    public void testCorrectExample() throws IOException {
        Assertions.assertEquals(5, linesCounterService.countLines(new File("test-classes/subfolder1/AbsolutelyCorrect.java"), FAKE_LIST, 0));
    }

    @Test
    public void testHiddenLineExample() throws IOException {
        Assertions.assertEquals(5, linesCounterService.countLines(new File("test-classes/subfolder2/HiddenLine.java"), FAKE_LIST, 0));
    }

    @Test
    public void testCustom() throws IOException {
        Assertions.assertEquals(5, linesCounterService.countLines(new File("test-classes/X.java"), FAKE_LIST, 0));
    }

    @Test
    public void testTotalExamples() throws IOException {
        Assertions.assertEquals(29, linesCounterService.countLines(new File("test-classes"), FAKE_LIST, 0));
    }
}
